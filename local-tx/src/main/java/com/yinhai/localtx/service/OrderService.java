/*
 *  Copyright 1999-2018 Alibaba Group Holding Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.yinhai.localtx.service;


import java.util.List;

import com.yinhai.localtx.Order;

/**
 * The interface Order service.
 */
public interface OrderService {
    /**
     * 创建订单
     *
     * @param userId        用户ID
     * @param commodityCode 商品编号
     * @param orderCount    订购数量
     * @return 生成的订单 order
     */
    Order create(String userId, String commodityCode, int orderCount);
    
    /**
     * 批量插入1w条记录测试
     *
     * @param userId
     * @param commodityCode
     * @param orderCount
     * @return
     */
    int insertBatchOrder(String userId,String commodityCode, int orderCount);
    
    /**
     * 批量更新1w条记录测试
     * @return
     */
    int updateBatchOrder();
    
    List<Order> queryAllOrder();
}
